package Service;

import java.util.ArrayList;
import model.Producto;
import model.Stock;

public class StockService 
{
	private Stock stock;
	
	public StockService() 
	{
		this.stock = new Stock();
	}
	
	public void addProducto(Producto p)
	{
		if(p != null)
			this.stock.getProductos().add(p);
		else
			System.out.println("Producto nulo");
	}
	
	public void mostrarProductos()
	{
		for (Producto producto : stock.getProductos()) 
		{
			System.out.println(producto);
		}
	}
	
	public void mostrarProductoMasBarato()
	{
		System.out.println("Producto m�s barato: " + getProductoMasBarato().getNombre());
	}
	
	public void mostrarProductoMasCaro()
	{
		System.out.println("Producto m�s caro: " + getProductoMasCaro().getNombre());
	}
	
	private Producto getProductoMasBarato()
	{
		ArrayList<Producto> productos = stock.getProductos();
		Producto masBarato = productos.get(0);
		for (int i = 1 ; i < productos.size() ; i++) 
		{
			if(masBarato.compareTo(productos.get(i).getPrecio()) > 0)
				masBarato = productos.get(i);
		}
		return masBarato;
	}
	
	private Producto getProductoMasCaro()
	{
		ArrayList<Producto> productos = stock.getProductos();
		Producto masCaro = productos.get(0);
		for (int i = 1 ; i < productos.size() ; i++) 
		{
			if(masCaro.compareTo(productos.get(i).getPrecio()) < 0)
				masCaro = productos.get(i);
		}
		return masCaro;
	}
}
