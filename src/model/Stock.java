package model;

import java.util.ArrayList;

public class Stock 
{
	private ArrayList<Producto> productos;
	
	public Stock() 
	{
		this.productos = new ArrayList<Producto>();
	}

	public ArrayList<Producto> getProductos() {
		return productos;
	}

	public void setProductos(ArrayList<Producto> productos) {
		this.productos = productos;
	}

	@Override
	public String toString() 
	{
		String ret = "";
		for (Producto producto : productos) 
		{
			ret += producto + "/n";
		}
		return ret;
	}
}
