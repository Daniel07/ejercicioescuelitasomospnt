package model;

public class Fruta extends Producto
{
	private String unidadDeVenta;
	
	public Fruta() 
	{
		super();
	}
	
	public Fruta(String nombre, int precio, String unidadDeVenta) 
	{
		super(nombre, precio);
		this.unidadDeVenta = unidadDeVenta;
	}

	public String getUnidadDeVenta() {
		return unidadDeVenta;
	}

	public void setUnidadDeVenta(String unidadDeVenta) {
		this.unidadDeVenta = unidadDeVenta;
	}

	@Override
	public String toString() 
	{
		return "Nombre: " + this.nombre + " /// Precio: $" + this.precio + " /// Unidad de venta: " + this.unidadDeVenta;
	}
	
	
}
