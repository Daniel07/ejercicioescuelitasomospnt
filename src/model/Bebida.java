package model;

public class Bebida extends Producto
{
	private double litro;
	
	public Bebida() 
	{
		super();
	}
	
	public Bebida(String nombre, int precio, double litro) 
	{
		super(nombre, precio);
		this.litro = litro;
	}

	public double getLitro() {
		return litro;
	}

	public void setLitro(double litro) {
		this.litro = litro;
	}

	@Override
	public String toString() 
	{
		return "Nombre: " + this.nombre + " /// Litros: " + this.litro + " /// Precio: $" + this.precio;
	}
	
	
}
