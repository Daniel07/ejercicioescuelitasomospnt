package model;

public class LimpiezaCapilar extends Producto
{
	private int contenido;
	
	public LimpiezaCapilar() 
	{
		super();
	}
	
	public LimpiezaCapilar(String nombre, int precio, int contenido) 
	{
		super(nombre, precio);
		this.contenido = contenido;
	}

	public int getContenido() {
		return contenido;
	}

	public void setContenido(int contenido) {
		this.contenido = contenido;
	}

	@Override
	public String toString() 
	{
		return "Nombre: " + this.nombre + " /// Contenido: " + this.contenido + "ml /// Precio: $" + this.precio;
	}
	
	
}
