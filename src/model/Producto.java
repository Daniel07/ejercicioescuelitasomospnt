package model;

public class Producto implements Comparable<Integer>
{
	protected String nombre;
	protected int precio;
	
	public Producto() 
	{
	}
	
	public Producto(String nombre, int precio) 
	{
		this.nombre = nombre;
		this.precio = precio;
	}
	
	public String getNombre() 
	{
		return nombre;
	}
	
	public void setNombre(String nombre) 
	{
		this.nombre = nombre;
	}
	
	public int getPrecio() 
	{
		return precio;
	}
	
	public void setPrecio(int precio) 
	{
		this.precio = precio;
	}

	@Override
	public String toString() 
	{
		return "Nombre: " + nombre + "/// Precio: " + precio;
	}

	@Override
	public int compareTo(Integer precio) 
	{
		return this.precio - precio;
	}
	
	
}
