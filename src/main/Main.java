package main;


import Service.StockService;
import model.Bebida;
import model.Fruta;
import model.LimpiezaCapilar;
import model.Producto;

public class Main {

	private static StockService stockService;
	
	public static void main(String[] args) 
	{
		stockService = new StockService();
		
		cargarProductos();
		
		stockService.mostrarProductos();
		System.out.println("=============================");
		stockService.mostrarProductoMasCaro();
		stockService.mostrarProductoMasBarato();
	}

	private static void cargarProductos() 
	{
		Producto p1 = new Bebida("Coca-Cola Zero", 20, 1.5);
		Producto p2 = new Bebida("Coca-Cola", 18, 1.5);
		Producto p3 = new LimpiezaCapilar("Shampoo Sedal", 19, 500);
		Producto p4 = new Fruta("Frutillas", 64, "kilo");
		
		stockService.addProducto(p1);
		stockService.addProducto(p2);
		stockService.addProducto(p3);
		stockService.addProducto(p4);
	}

}
